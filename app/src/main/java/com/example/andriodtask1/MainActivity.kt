package com.example.andriodtask1

import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isInvisible
import java.util.*
import kotlin.concurrent.schedule
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    companion object {
        var isAddScore :Boolean = false
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainGame()
    }

    private fun mainGame() {
        isAddScore = false
        resetBackground()
        val numberResult: Int = createQuestionAndResult()
        createChoice(numberResult)
        resetComponent()
    }

    private fun createQuestionAndResult() : Int{
        val txtNumber1 = findViewById<TextView>(R.id.txtNumber1)
        val txtNumber2 = findViewById<TextView>(R.id.txtNumber2)

        val number1:Int = Random.nextInt(0, 10)
        val number2:Int = Random.nextInt(0, 10)
        val numberResult:Int = number1 + number2

        txtNumber1.text = number1.toString()
        txtNumber2.text = number2.toString()
//        Toast.makeText(this,"${txtNumber1.text} ",Toast.LENGTH_SHORT).show()
//        Toast.makeText(this,"${txtNumber2.text} ",Toast.LENGTH_SHORT).show()
        return numberResult
    }
    private fun createChoice(numberResult:Int){
        val btnAnswer1 = findViewById<Button>(R.id.btnAnswer1)
        val btnAnswer2 = findViewById<Button>(R.id.btnAnswer2)
        val btnAnswer3 = findViewById<Button>(R.id.btnAnswer3)
        val position = Random.nextInt(1, 4)
        if (position === 1) {
            btnAnswer1.text = numberResult.toString()
            btnAnswer2.text  = (numberResult + 1).toString()
            btnAnswer3.text  = (numberResult + 2).toString()
        } else if (position === 2) {
            btnAnswer1.text  = (numberResult - 1).toString()
            btnAnswer2.text  = (numberResult).toString()
            btnAnswer3.text  = (numberResult + 1).toString()
        } else if (position === 3) {
            btnAnswer1.text  = (numberResult - 2).toString()
            btnAnswer2.text  = (numberResult - 1).toString()
            btnAnswer3.text  = (numberResult).toString()
        }
    }
    private fun resetComponent(){
        val txtTime = findViewById<TextView>(R.id.txtTime)
        val txtNumberResult = findViewById<TextView>(R.id.txtResult)
        txtNumberResult.text = ""
        txtTime.text = ""
    }

    private fun addAmountCorrect(){
        val txtAmountCorrect = findViewById<TextView>(R.id.txtAmountCorrect)
        txtAmountCorrect.text = (txtAmountCorrect.text.toString().toInt() +1).toString()
        isAddScore = true
    }
    private fun addAmountWrong(){
        val txtAmountWrong = findViewById<TextView>(R.id.txtAmountWrong)
        txtAmountWrong.text = (txtAmountWrong.text.toString().toInt() +1).toString()
        isAddScore = true
    }
    private fun resetBackground(){
        val txtResult = findViewById<TextView>(R.id.txtResult)
        txtResult.setTextColor(Color.BLACK)
        val btnAnswer1 = findViewById<Button>(R.id.btnAnswer1)
        btnAnswer1.setBackgroundColor(Color.LTGRAY)
        val btnAnswer2 = findViewById<Button>(R.id.btnAnswer2)
        btnAnswer2.setBackgroundColor(Color.LTGRAY)
        val btnAnswer3 = findViewById<Button>(R.id.btnAnswer3)
        btnAnswer3.setBackgroundColor(Color.LTGRAY)
    }
    private fun changeBackgroundWrong(btn:Button){
        val txtResult = findViewById<TextView>(R.id.txtResult)
        txtResult.setTextColor(Color.RED)
        btn.setBackgroundColor(Color.RED)
    }
    private fun changeBackgroundCorrect(btn:Button){
        val txtResult = findViewById<TextView>(R.id.txtResult)
        txtResult.setTextColor(Color.GREEN)
        btn.setBackgroundColor(Color.GREEN)
    }

    fun onSelectAnswer(view: View){
        val txtNumber1 = findViewById<TextView>(R.id.txtNumber1)
        val txtNumber2 = findViewById<TextView>(R.id.txtNumber2)
        val txtNumberResult = findViewById<TextView>(R.id.txtResult)
        val btn = (view as Button)
        if(btn.text.toString().toInt() === txtNumber1.text.toString().toInt() +  txtNumber2.text.toString().toInt() ){
            txtNumberResult.text = "ถูกต้อง"
            if(!isAddScore){
                addAmountCorrect()
            }
            changeBackgroundCorrect(btn)
            val timer = object: CountDownTimer(3000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    val txtTime = findViewById<TextView>(R.id.txtTime)
                    txtTime.text = "ข้อต่อไปใน ${(millisUntilFinished/1000).toInt()}"
                }
                override fun onFinish() {
                    mainGame()
                }
            }
            timer.start()
        }else{
            txtNumberResult.text = "${btn.text} ผิดจ้า เลือกใหม่นะ"
            if(!isAddScore){
                addAmountWrong()
            }
            changeBackgroundWrong(btn)
        }
    }
}